﻿/*
Что нужно сделать
Решите задачу «Встреча животных».
Встречаясь, животные реагируют друг на друга. Реализуйте
базовый класс Animal, классы-наследники Dog и Cat, и напишите
функцию meeting, принимающую два указателя на базовый класс,
по которому лежат элементы классов-наследников, которая
выводит в консоль разные фразы в зависимости от того, какие
конкретные классы встретились.

Dog + Dog → “Woof-Woof”
Dog + Cat → “Bark Meow”
Cat + Dog → “Meow Bark”
Cat + Cat → “Purr Purr”

Пример:
Animal* a = new Dog();
Animal* b = new Cat();
meeting(a,b);

Вывод:
Bark Meow


Чек-лист для проверки задачи
    Функция принимает корректные типы данных, тип
    возвращаемого значения — void.
    Решение выполнено через двойную диспетчеризацию или
    кастование к базовому классу.
    Сигнатура функции void meeting (Animal* a, Animal* b),
    любая другая некорректна.
*/

#include <iostream>
#include <string>

class Animal {
public:
    Animal() {};
    ~Animal() {};
    virtual void meeting(Animal* a, Animal* b) {};
};

class Dog : public Animal {
public:
    Dog() {};
    ~Dog() {};
};

class Cat : public Animal {
public:
    Cat() {};
    ~Cat() {};
};

void meeting(Animal* a, Animal* b) {

    Animal* aa = dynamic_cast<Dog*>(a);
    Animal* bb = dynamic_cast<Cat*>(b);

    if (aa && bb) {
        std::cout << "Bark Meow\n";
    }
    else if (!aa && !bb) {
        std::cout << "Meow Bark\n";
    }
    else if (aa && !bb) {
        std::cout << "Woof Woof\n";
    }
    else if (!aa && bb) {
        std::cout << "Purr Purr\n";
    }

}

int main()
{
    Animal* a = new Cat();
    Animal* b = new Dog();

    meeting(a, b);

    delete a;
    delete b;
}
